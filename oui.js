
document.addEventListener('DOMContentLoaded', function()
{
  var dice = document.getElementById('dice'),
    diceRes = document.getElementsByClassName('oui')[0],
    nbofRoll,
    nbofFace,
    inter,
    to;

  diceRes.addEventListener('mousedown', function()
  {
    nbofFace = parseInt(choix.innerText);
    if(nbofFace > 1)
    {
      nbofRoll = 20 + rng(20);
      roll(nbofFace,nbofRoll);
    }
    else
    {
      error.innerText = 'Il faut rentrer un nombre entier supérieur à 1 (en chiffres évidemment)'
    }
  });

  document.addEventListener('keydown', function(event)
  {
    if(event.keyCode == 13)
    {
      event.preventDefault();
    }
  });

  function rng(n)
  {
    return Math.ceil(Math.random() * n);
  }

  function addNb(n)
  {
    console.log(n);
    let sound = new Audio('WheelSound.mp3');
    sound.volume = 0.5;
    sound.play();

    let next = document.createElement('div');
    next.classList.add('oui');
    next.style.setProperty('height','0vh');
    next.style.setProperty('order','0');
    document.getElementById('dice').appendChild(next);

    next.style.setProperty('height','25vh');
    diceRes.style.setProperty('height','0vh');
    diceRes.style.setProperty('order','1');

    next.innerHTML = rng(n);
    next.addEventListener('mousedown', function()
    {
      nbofFace = parseInt(choix.innerText);
      if(nbofFace > 1)
      {
        nbofRoll = 20 + rng(20);
        roll(nbofFace,nbofRoll);
      }
      else
      {
        error.innerText = 'Il faut rentrer un nombre entier supérieur à 1 (en chiffres évidemment)'
      }
    });

    diceRes = document.getElementsByClassName('oui')[1];
  }

  function roll(faces,i)
  {
    if(i > 35/100*nbofRoll)
    {
      i--;

      addNb(faces);

      setTimeout(function()
      {
        document.getElementsByClassName('oui')[0].style.setProperty('order','1');
        dice.removeChild(document.getElementsByClassName('oui')[0]);
        roll(faces,i);
      }, 100);
    }
    else if(i > 20/100*nbofRoll)
    {
      i--;

      addNb(faces);

      setTimeout(function()
      {
        document.getElementsByClassName('oui')[0].style.setProperty('order','1');
        dice.removeChild(document.getElementsByClassName('oui')[0]);
        roll(faces,i);
      }, 200);
    }
    else if(i > 10/100*nbofRoll)
    {
      i--;

      addNb(faces);

      setTimeout(function()
      {
        document.getElementsByClassName('oui')[0].style.setProperty('order','1');
        dice.removeChild(document.getElementsByClassName('oui')[0]);
        roll(faces,i);
      }, 300);
    }
    else if(i > 0)
    {
      i--;

      addNb(faces);

      setTimeout(function()
      {
        document.getElementsByClassName('oui')[0].style.setProperty('order','1');
        dice.removeChild(document.getElementsByClassName('oui')[0]);
        roll(faces,i);
      }, 500);
    }
    else
    {
      let sound = new Audio('WOW.mp3');
      sound.volume = 0.8;
      sound.play();

      diceRes.style.setProperty('font-size', '12vh');
      diceRes.style.setProperty('text-shadow', '0 0 1vh #666');
      setTimeout(function()
      {
        diceRes.style.setProperty('font-size', '10vh');
        diceRes.style.setProperty('text-shadow', '0 0 0 #666');
      }, 200);
    }
  }
});
